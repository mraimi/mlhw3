function distances = get_sq_dist( X,Y )
    distances = pdist2(X,Y).^2;
end

