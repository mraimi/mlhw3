lambda = exp(-10);
h = 33.3935598039869;
load('std_training_data.mat');
load('std_testdata.mat');
load('spam.mat');

train_kernel = prod_kern(get_dist(std_data,std_data),h);
test_kernel = prod_kern(get_dist(std_testdata,std_data),h);

alpha = hw3_train_ksvm(train_kernel,labels,lambda);
preds = hw3_test_ksvm(alpha,train_kernel,labels);
train_error = pred_eval(preds, labels);
save('train_error.mat','train_error');
 
preds = hw3_test_ksvm(alpha,test_kernel,labels);
test_error = pred_eval(preds, testlabels);
save('test_error.mat','test_error');

plot(1:length(alpha), sort(alpha,'descend'))