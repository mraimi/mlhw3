function output = prod_kern( distances, h )
    output = -(distances/(2*h));
    output = exp(output);
end

