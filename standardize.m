function output = standardize( data )
        output = data;
        output = bsxfun(@minus, output, mean(data));
        output = bsxfun(@rdivide,output,std(data));
end