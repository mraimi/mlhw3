load('spam.mat');
load('kfold_indices.mat');
load('std_training_data.mat');
errors = zeros(11,5);
quant_vals = get_quantiles(std_data);

for i=0:10 %loop for different lamba settings
    for h=1:5
        lambda = exp(-i);
        errors(i+1,h) = five_folds_valid(lambda,quant_vals(1,h));
    end
end
save('error_output.mat','errors');