function err = pred_eval( x, y )
    confusion = zeros(2,2);
    truePos = 0;
    falsePos = 0;
    trueNeg = 0;
    falseNeg = 0;
    
    if length(x)~=length(y)
        error('eval error. mismatch dimensions for predictions and labels');
    end
    for i=1:length(x)
        if y(i,1) == 1
            if x(i,1) == 1
                truePos = truePos + 1;
            else
                falseNeg = falseNeg + 1;
            end
        elseif y(i,1) == -1
            if x(i,1) == -1
                trueNeg = trueNeg + 1;
            else
                falsePos = falsePos + 1;
            end
        end   
    end
    err = (falsePos + falseNeg)/length(x);
    
    if (truePos+falsePos+trueNeg+falseNeg)~=length(x)
        error('DOES NOT ADD UP');
    end
    
    truePos2 = truePos/(truePos+falseNeg);
    falsePos2 = falsePos/(falsePos+trueNeg);
    trueNeg2 = trueNeg/(trueNeg+falsePos);
    falseNeg2 = falseNeg/(falseNeg+truePos);
    
    confusion(1,1) = trueNeg2;
    confusion(1,2) = falsePos2;
    confusion(2,1) = falseNeg2;
    confusion(2,2) = truePos2;
    
    
    
    save('confusionMatrix.mat','confusion');
end

