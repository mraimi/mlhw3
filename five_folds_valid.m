function error = five_folds_valid( lambda, h )
%FIVE_FOLDS_VALID Summary of this function goes here
%   Detailed explanation goes here
    load('kfold_indices.mat');
    load('std_training_data.mat');
    load('spam.mat');
    indices = kfold_indices;
    error = 0;
    for i=1:5
        logical_val = (indices == i);
        logical_train = ~logical_val;
        
        val_data = std_data(logical_val,:);
        train_data = std_data(logical_train,:);
        
        train_lbls = labels(logical_train);
        val_lbls = labels(logical_val);
        
        train_kernel = prod_kern(get_dist(train_data,train_data),h);
        val_kernel = prod_kern(get_dist(train_data,val_data),h);
        
        alpha = hw3_train_ksvm(train_kernel,train_lbls,lambda);
       	preds = hw3_test_ksvm(alpha,val_kernel',train_lbls);
        error = error + pred_eval(val_lbls, preds);
    end
    error = error/5;
end

