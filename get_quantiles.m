function output = get_quantiles( data )
    quant_vals = [.1 .25 .5 .75 .9];
    output = zeros(1,length(quant_vals));
    distances = get_dist(data,data);
    for i=1:length(quant_vals)
        output(1,i) = quantile(distances(:),quant_vals(1,i));
    end
end

